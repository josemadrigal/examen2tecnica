package examen.web.rest;

import examen.Examen2TecnicaApp;

import examen.domain.Proyect;
import examen.domain.Period;
import examen.repository.ProyectRepository;
import examen.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static examen.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ProyectResource REST controller.
 *
 * @see ProyectResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Examen2TecnicaApp.class)
public class ProyectResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_NUM_STUDENT = 1;
    private static final Integer UPDATED_NUM_STUDENT = 2;

    private static final String DEFAULT_SCHEDULE = "AAAAAAAAAA";
    private static final String UPDATED_SCHEDULE = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    @Autowired
    private ProyectRepository proyectRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restProyectMockMvc;

    private Proyect proyect;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProyectResource proyectResource = new ProyectResource(proyectRepository);
        this.restProyectMockMvc = MockMvcBuilders.standaloneSetup(proyectResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Proyect createEntity(EntityManager em) {
        Proyect proyect = new Proyect()
            .name(DEFAULT_NAME)
            .numStudent(DEFAULT_NUM_STUDENT)
            .schedule(DEFAULT_SCHEDULE)
            .status(DEFAULT_STATUS);
        // Add required entity
        Period period = PeriodResourceIntTest.createEntity(em);
        em.persist(period);
        em.flush();
        proyect.setPeriodo(period);
        return proyect;
    }

    @Before
    public void initTest() {
        proyect = createEntity(em);
    }

    @Test
    @Transactional
    public void createProyect() throws Exception {
        int databaseSizeBeforeCreate = proyectRepository.findAll().size();

        // Create the Proyect
        restProyectMockMvc.perform(post("/api/proyects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(proyect)))
            .andExpect(status().isCreated());

        // Validate the Proyect in the database
        List<Proyect> proyectList = proyectRepository.findAll();
        assertThat(proyectList).hasSize(databaseSizeBeforeCreate + 1);
        Proyect testProyect = proyectList.get(proyectList.size() - 1);
        assertThat(testProyect.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testProyect.getNumStudent()).isEqualTo(DEFAULT_NUM_STUDENT);
        assertThat(testProyect.getSchedule()).isEqualTo(DEFAULT_SCHEDULE);
        assertThat(testProyect.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createProyectWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = proyectRepository.findAll().size();

        // Create the Proyect with an existing ID
        proyect.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProyectMockMvc.perform(post("/api/proyects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(proyect)))
            .andExpect(status().isBadRequest());

        // Validate the Proyect in the database
        List<Proyect> proyectList = proyectRepository.findAll();
        assertThat(proyectList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllProyects() throws Exception {
        // Initialize the database
        proyectRepository.saveAndFlush(proyect);

        // Get all the proyectList
        restProyectMockMvc.perform(get("/api/proyects?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(proyect.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].numStudent").value(hasItem(DEFAULT_NUM_STUDENT)))
            .andExpect(jsonPath("$.[*].schedule").value(hasItem(DEFAULT_SCHEDULE.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }
    
    @Test
    @Transactional
    public void getProyect() throws Exception {
        // Initialize the database
        proyectRepository.saveAndFlush(proyect);

        // Get the proyect
        restProyectMockMvc.perform(get("/api/proyects/{id}", proyect.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(proyect.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.numStudent").value(DEFAULT_NUM_STUDENT))
            .andExpect(jsonPath("$.schedule").value(DEFAULT_SCHEDULE.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingProyect() throws Exception {
        // Get the proyect
        restProyectMockMvc.perform(get("/api/proyects/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProyect() throws Exception {
        // Initialize the database
        proyectRepository.saveAndFlush(proyect);

        int databaseSizeBeforeUpdate = proyectRepository.findAll().size();

        // Update the proyect
        Proyect updatedProyect = proyectRepository.findById(proyect.getId()).get();
        // Disconnect from session so that the updates on updatedProyect are not directly saved in db
        em.detach(updatedProyect);
        updatedProyect
            .name(UPDATED_NAME)
            .numStudent(UPDATED_NUM_STUDENT)
            .schedule(UPDATED_SCHEDULE)
            .status(UPDATED_STATUS);

        restProyectMockMvc.perform(put("/api/proyects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedProyect)))
            .andExpect(status().isOk());

        // Validate the Proyect in the database
        List<Proyect> proyectList = proyectRepository.findAll();
        assertThat(proyectList).hasSize(databaseSizeBeforeUpdate);
        Proyect testProyect = proyectList.get(proyectList.size() - 1);
        assertThat(testProyect.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testProyect.getNumStudent()).isEqualTo(UPDATED_NUM_STUDENT);
        assertThat(testProyect.getSchedule()).isEqualTo(UPDATED_SCHEDULE);
        assertThat(testProyect.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingProyect() throws Exception {
        int databaseSizeBeforeUpdate = proyectRepository.findAll().size();

        // Create the Proyect

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProyectMockMvc.perform(put("/api/proyects")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(proyect)))
            .andExpect(status().isBadRequest());

        // Validate the Proyect in the database
        List<Proyect> proyectList = proyectRepository.findAll();
        assertThat(proyectList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProyect() throws Exception {
        // Initialize the database
        proyectRepository.saveAndFlush(proyect);

        int databaseSizeBeforeDelete = proyectRepository.findAll().size();

        // Delete the proyect
        restProyectMockMvc.perform(delete("/api/proyects/{id}", proyect.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Proyect> proyectList = proyectRepository.findAll();
        assertThat(proyectList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Proyect.class);
        Proyect proyect1 = new Proyect();
        proyect1.setId(1L);
        Proyect proyect2 = new Proyect();
        proyect2.setId(proyect1.getId());
        assertThat(proyect1).isEqualTo(proyect2);
        proyect2.setId(2L);
        assertThat(proyect1).isNotEqualTo(proyect2);
        proyect1.setId(null);
        assertThat(proyect1).isNotEqualTo(proyect2);
    }
}
