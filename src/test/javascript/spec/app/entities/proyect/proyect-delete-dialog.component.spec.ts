/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { Examen2TecnicaTestModule } from '../../../test.module';
import { ProyectDeleteDialogComponent } from 'app/entities/proyect/proyect-delete-dialog.component';
import { ProyectService } from 'app/entities/proyect/proyect.service';

describe('Component Tests', () => {
    describe('Proyect Management Delete Component', () => {
        let comp: ProyectDeleteDialogComponent;
        let fixture: ComponentFixture<ProyectDeleteDialogComponent>;
        let service: ProyectService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [Examen2TecnicaTestModule],
                declarations: [ProyectDeleteDialogComponent]
            })
                .overrideTemplate(ProyectDeleteDialogComponent, '')
                .compileComponents();
            fixture = TestBed.createComponent(ProyectDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProyectService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete', inject(
                [],
                fakeAsync(() => {
                    // GIVEN
                    spyOn(service, 'delete').and.returnValue(of({}));

                    // WHEN
                    comp.confirmDelete(123);
                    tick();

                    // THEN
                    expect(service.delete).toHaveBeenCalledWith(123);
                    expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                })
            ));
        });
    });
});
