package examen.web.rest;
import examen.domain.Proyect;
import examen.repository.ProyectRepository;
import examen.web.rest.errors.BadRequestAlertException;
import examen.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Proyect.
 */
@RestController
@RequestMapping("/api")
public class ProyectResource {

    private final Logger log = LoggerFactory.getLogger(ProyectResource.class);

    private static final String ENTITY_NAME = "proyect";

    private final ProyectRepository proyectRepository;

    public ProyectResource(ProyectRepository proyectRepository) {
        this.proyectRepository = proyectRepository;
    }

    /**
     * POST  /proyects : Create a new proyect.
     *
     * @param proyect the proyect to create
     * @return the ResponseEntity with status 201 (Created) and with body the new proyect, or with status 400 (Bad Request) if the proyect has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/proyects")
    public ResponseEntity<Proyect> createProyect(@Valid @RequestBody Proyect proyect) throws URISyntaxException {
        log.debug("REST request to save Proyect : {}", proyect);
        if (proyect.getId() != null) {
            throw new BadRequestAlertException("A new proyect cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Proyect result = proyectRepository.save(proyect);
        return ResponseEntity.created(new URI("/api/proyects/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /proyects : Updates an existing proyect.
     *
     * @param proyect the proyect to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated proyect,
     * or with status 400 (Bad Request) if the proyect is not valid,
     * or with status 500 (Internal Server Error) if the proyect couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/proyects")
    public ResponseEntity<Proyect> updateProyect(@Valid @RequestBody Proyect proyect) throws URISyntaxException {
        log.debug("REST request to update Proyect : {}", proyect);
        if (proyect.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Proyect result = proyectRepository.save(proyect);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, proyect.getId().toString()))
            .body(result);
    }

    /**
     * GET  /proyects : get all the proyects.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of proyects in body
     */
    @GetMapping("/proyects")
    public List<Proyect> getAllProyects() {
        log.debug("REST request to get all Proyects");
        return proyectRepository.findAll();
    }

    /**
     * GET  /proyects/:id : get the "id" proyect.
     *
     * @param id the id of the proyect to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the proyect, or with status 404 (Not Found)
     */
    @GetMapping("/proyects/{id}")
    public ResponseEntity<Proyect> getProyect(@PathVariable Long id) {
        log.debug("REST request to get Proyect : {}", id);
        Optional<Proyect> proyect = proyectRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(proyect);
    }

    /**
     * DELETE  /proyects/:id : delete the "id" proyect.
     *
     * @param id the id of the proyect to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/proyects/{id}")
    public ResponseEntity<Void> deleteProyect(@PathVariable Long id) {
        log.debug("REST request to delete Proyect : {}", id);
        proyectRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
