package examen.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Proyect.
 */
@Entity
@Table(name = "proyect")
public class Proyect implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "num_student")
    private Integer numStudent;

    @Column(name = "schedule")
    private String schedule;

    @Column(name = "status")
    private String status;

    @OneToMany(mappedBy = "proyecto")
    private Set<Team> teams = new HashSet<>();
    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("proyects")
    private Period periodo;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Proyect name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumStudent() {
        return numStudent;
    }

    public Proyect numStudent(Integer numStudent) {
        this.numStudent = numStudent;
        return this;
    }

    public void setNumStudent(Integer numStudent) {
        this.numStudent = numStudent;
    }

    public String getSchedule() {
        return schedule;
    }

    public Proyect schedule(String schedule) {
        this.schedule = schedule;
        return this;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getStatus() {
        return status;
    }

    public Proyect status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Set<Team> getTeams() {
        return teams;
    }

    public Proyect teams(Set<Team> teams) {
        this.teams = teams;
        return this;
    }

    public Proyect addTeam(Team team) {
        this.teams.add(team);
        team.setProyecto(this);
        return this;
    }

    public Proyect removeTeam(Team team) {
        this.teams.remove(team);
        team.setProyecto(null);
        return this;
    }

    public void setTeams(Set<Team> teams) {
        this.teams = teams;
    }

    public Period getPeriodo() {
        return periodo;
    }

    public Proyect periodo(Period period) {
        this.periodo = period;
        return this;
    }

    public void setPeriodo(Period period) {
        this.periodo = period;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Proyect proyect = (Proyect) o;
        if (proyect.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), proyect.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Proyect{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", numStudent=" + getNumStudent() +
            ", schedule='" + getSchedule() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
