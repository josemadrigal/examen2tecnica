package examen.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Review.
 */
@Entity
@Table(name = "review")
public class Review implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "jhi_timestamp")
    private LocalDate timestamp;

    @Column(name = "commnet")
    private String commnet;

    @Column(name = "status")
    private String status;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("reviews")
    private Story codigoHU;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getTimestamp() {
        return timestamp;
    }

    public Review timestamp(LocalDate timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public void setTimestamp(LocalDate timestamp) {
        this.timestamp = timestamp;
    }

    public String getCommnet() {
        return commnet;
    }

    public Review commnet(String commnet) {
        this.commnet = commnet;
        return this;
    }

    public void setCommnet(String commnet) {
        this.commnet = commnet;
    }

    public String getStatus() {
        return status;
    }

    public Review status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Story getCodigoHU() {
        return codigoHU;
    }

    public Review codigoHU(Story story) {
        this.codigoHU = story;
        return this;
    }

    public void setCodigoHU(Story story) {
        this.codigoHU = story;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Review review = (Review) o;
        if (review.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), review.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Review{" +
            "id=" + getId() +
            ", timestamp='" + getTimestamp() + "'" +
            ", commnet='" + getCommnet() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
