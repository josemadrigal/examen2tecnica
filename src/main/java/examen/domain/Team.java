package examen.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Team.
 */
@Entity
@Table(name = "team")
public class Team implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "status")
    private String status;

    @OneToMany(mappedBy = "equipo")
    private Set<Student> students = new HashSet<>();
    @OneToMany(mappedBy = "equipo")
    private Set<Sprint> springs = new HashSet<>();
    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("teams")
    private Proyect proyecto;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Team name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public Team status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public Team students(Set<Student> students) {
        this.students = students;
        return this;
    }

    public Team addStudent(Student student) {
        this.students.add(student);
        student.setEquipo(this);
        return this;
    }

    public Team removeStudent(Student student) {
        this.students.remove(student);
        student.setEquipo(null);
        return this;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

    public Set<Sprint> getSprings() {
        return springs;
    }

    public Team springs(Set<Sprint> sprints) {
        this.springs = sprints;
        return this;
    }

    public Team addSpring(Sprint sprint) {
        this.springs.add(sprint);
        sprint.setEquipo(this);
        return this;
    }

    public Team removeSpring(Sprint sprint) {
        this.springs.remove(sprint);
        sprint.setEquipo(null);
        return this;
    }

    public void setSprings(Set<Sprint> sprints) {
        this.springs = sprints;
    }

    public Proyect getProyecto() {
        return proyecto;
    }

    public Team proyecto(Proyect proyect) {
        this.proyecto = proyect;
        return this;
    }

    public void setProyecto(Proyect proyect) {
        this.proyecto = proyect;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Team team = (Team) o;
        if (team.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), team.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Team{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
