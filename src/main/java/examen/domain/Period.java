package examen.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Period.
 */
@Entity
@Table(name = "period")
public class Period implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "endate")
    private LocalDate endate;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "periodo")
    private Set<Proyect> proyects = new HashSet<>();
    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public Period startDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndate() {
        return endate;
    }

    public Period endate(LocalDate endate) {
        this.endate = endate;
        return this;
    }

    public void setEndate(LocalDate endate) {
        this.endate = endate;
    }

    public String getName() {
        return name;
    }

    public Period name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Proyect> getProyects() {
        return proyects;
    }

    public Period proyects(Set<Proyect> proyects) {
        this.proyects = proyects;
        return this;
    }

    public Period addProyect(Proyect proyect) {
        this.proyects.add(proyect);
        proyect.setPeriodo(this);
        return this;
    }

    public Period removeProyect(Proyect proyect) {
        this.proyects.remove(proyect);
        proyect.setPeriodo(null);
        return this;
    }

    public void setProyects(Set<Proyect> proyects) {
        this.proyects = proyects;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Period period = (Period) o;
        if (period.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), period.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Period{" +
            "id=" + getId() +
            ", startDate='" + getStartDate() + "'" +
            ", endate='" + getEndate() + "'" +
            ", name='" + getName() + "'" +
            "}";
    }
}
