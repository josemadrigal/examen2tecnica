import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { Examen2TecnicaSharedModule } from 'app/shared';
import {
    ProyectComponent,
    ProyectDetailComponent,
    ProyectUpdateComponent,
    ProyectDeletePopupComponent,
    ProyectDeleteDialogComponent,
    proyectRoute,
    proyectPopupRoute
} from './';

const ENTITY_STATES = [...proyectRoute, ...proyectPopupRoute];

@NgModule({
    imports: [Examen2TecnicaSharedModule, RouterModule.forChild(ENTITY_STATES)],
    declarations: [
        ProyectComponent,
        ProyectDetailComponent,
        ProyectUpdateComponent,
        ProyectDeleteDialogComponent,
        ProyectDeletePopupComponent
    ],
    entryComponents: [ProyectComponent, ProyectUpdateComponent, ProyectDeleteDialogComponent, ProyectDeletePopupComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Examen2TecnicaProyectModule {}
