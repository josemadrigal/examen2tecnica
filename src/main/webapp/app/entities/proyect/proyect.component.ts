import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IProyect } from 'app/shared/model/proyect.model';
import { AccountService } from 'app/core';
import { ProyectService } from './proyect.service';

@Component({
    selector: 'jhi-proyect',
    templateUrl: './proyect.component.html'
})
export class ProyectComponent implements OnInit, OnDestroy {
    proyects: IProyect[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        protected proyectService: ProyectService,
        protected jhiAlertService: JhiAlertService,
        protected eventManager: JhiEventManager,
        protected accountService: AccountService
    ) {}

    loadAll() {
        this.proyectService
            .query()
            .pipe(
                filter((res: HttpResponse<IProyect[]>) => res.ok),
                map((res: HttpResponse<IProyect[]>) => res.body)
            )
            .subscribe(
                (res: IProyect[]) => {
                    this.proyects = res;
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
    }

    ngOnInit() {
        this.loadAll();
        this.accountService.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInProyects();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IProyect) {
        return item.id;
    }

    registerChangeInProyects() {
        this.eventSubscriber = this.eventManager.subscribe('proyectListModification', response => this.loadAll());
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
