import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IProyect } from 'app/shared/model/proyect.model';
import { ProyectService } from './proyect.service';

@Component({
    selector: 'jhi-proyect-delete-dialog',
    templateUrl: './proyect-delete-dialog.component.html'
})
export class ProyectDeleteDialogComponent {
    proyect: IProyect;

    constructor(protected proyectService: ProyectService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.proyectService.delete(id).subscribe(response => {
            this.eventManager.broadcast({
                name: 'proyectListModification',
                content: 'Deleted an proyect'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-proyect-delete-popup',
    template: ''
})
export class ProyectDeletePopupComponent implements OnInit, OnDestroy {
    protected ngbModalRef: NgbModalRef;

    constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

    ngOnInit() {
        this.activatedRoute.data.subscribe(({ proyect }) => {
            setTimeout(() => {
                this.ngbModalRef = this.modalService.open(ProyectDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
                this.ngbModalRef.componentInstance.proyect = proyect;
                this.ngbModalRef.result.then(
                    result => {
                        this.router.navigate(['/proyect', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    },
                    reason => {
                        this.router.navigate(['/proyect', { outlets: { popup: null } }]);
                        this.ngbModalRef = null;
                    }
                );
            }, 0);
        });
    }

    ngOnDestroy() {
        this.ngbModalRef = null;
    }
}
