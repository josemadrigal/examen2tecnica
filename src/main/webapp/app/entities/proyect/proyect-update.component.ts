import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IProyect } from 'app/shared/model/proyect.model';
import { ProyectService } from './proyect.service';
import { IPeriod } from 'app/shared/model/period.model';
import { PeriodService } from 'app/entities/period';

@Component({
    selector: 'jhi-proyect-update',
    templateUrl: './proyect-update.component.html'
})
export class ProyectUpdateComponent implements OnInit {
    proyect: IProyect;
    isSaving: boolean;

    periods: IPeriod[];

    constructor(
        protected jhiAlertService: JhiAlertService,
        protected proyectService: ProyectService,
        protected periodService: PeriodService,
        protected activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ proyect }) => {
            this.proyect = proyect;
        });
        this.periodService
            .query()
            .pipe(
                filter((mayBeOk: HttpResponse<IPeriod[]>) => mayBeOk.ok),
                map((response: HttpResponse<IPeriod[]>) => response.body)
            )
            .subscribe((res: IPeriod[]) => (this.periods = res), (res: HttpErrorResponse) => this.onError(res.message));
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.proyect.id !== undefined) {
            this.subscribeToSaveResponse(this.proyectService.update(this.proyect));
        } else {
            this.subscribeToSaveResponse(this.proyectService.create(this.proyect));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<IProyect>>) {
        result.subscribe((res: HttpResponse<IProyect>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    protected onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    protected onSaveError() {
        this.isSaving = false;
    }

    protected onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackPeriodById(index: number, item: IPeriod) {
        return item.id;
    }
}
