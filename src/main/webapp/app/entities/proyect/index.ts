export * from './proyect.service';
export * from './proyect-update.component';
export * from './proyect-delete-dialog.component';
export * from './proyect-detail.component';
export * from './proyect.component';
export * from './proyect.route';
