import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Proyect } from 'app/shared/model/proyect.model';
import { ProyectService } from './proyect.service';
import { ProyectComponent } from './proyect.component';
import { ProyectDetailComponent } from './proyect-detail.component';
import { ProyectUpdateComponent } from './proyect-update.component';
import { ProyectDeletePopupComponent } from './proyect-delete-dialog.component';
import { IProyect } from 'app/shared/model/proyect.model';

@Injectable({ providedIn: 'root' })
export class ProyectResolve implements Resolve<IProyect> {
    constructor(private service: ProyectService) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IProyect> {
        const id = route.params['id'] ? route.params['id'] : null;
        if (id) {
            return this.service.find(id).pipe(
                filter((response: HttpResponse<Proyect>) => response.ok),
                map((proyect: HttpResponse<Proyect>) => proyect.body)
            );
        }
        return of(new Proyect());
    }
}

export const proyectRoute: Routes = [
    {
        path: '',
        component: ProyectComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Proyects'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/view',
        component: ProyectDetailComponent,
        resolve: {
            proyect: ProyectResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Proyects'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'new',
        component: ProyectUpdateComponent,
        resolve: {
            proyect: ProyectResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Proyects'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: ':id/edit',
        component: ProyectUpdateComponent,
        resolve: {
            proyect: ProyectResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Proyects'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const proyectPopupRoute: Routes = [
    {
        path: ':id/delete',
        component: ProyectDeletePopupComponent,
        resolve: {
            proyect: ProyectResolve
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Proyects'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
