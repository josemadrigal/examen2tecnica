import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'period',
                loadChildren: './period/period.module#Examen2TecnicaPeriodModule'
            },
            {
                path: 'proyect',
                loadChildren: './proyect/proyect.module#Examen2TecnicaProyectModule'
            },
            {
                path: 'team',
                loadChildren: './team/team.module#Examen2TecnicaTeamModule'
            },
            {
                path: 'student',
                loadChildren: './student/student.module#Examen2TecnicaStudentModule'
            },
            {
                path: 'sprint',
                loadChildren: './sprint/sprint.module#Examen2TecnicaSprintModule'
            },
            {
                path: 'story',
                loadChildren: './story/story.module#Examen2TecnicaStoryModule'
            },
            {
                path: 'review',
                loadChildren: './review/review.module#Examen2TecnicaReviewModule'
            }
            /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
        ])
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Examen2TecnicaEntityModule {}
