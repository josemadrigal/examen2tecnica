import { NgModule } from '@angular/core';

import { Examen2TecnicaSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [Examen2TecnicaSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [Examen2TecnicaSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class Examen2TecnicaSharedCommonModule {}
