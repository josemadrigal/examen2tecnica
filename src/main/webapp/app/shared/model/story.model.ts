import { IStudent } from 'app/shared/model/student.model';
import { IReview } from 'app/shared/model/review.model';
import { ISprint } from 'app/shared/model/sprint.model';

export interface IStory {
    id?: number;
    code?: string;
    name?: string;
    descripcion?: string;
    status?: string;
    students?: IStudent[];
    reviews?: IReview[];
    iteracion?: ISprint;
}

export class Story implements IStory {
    constructor(
        public id?: number,
        public code?: string,
        public name?: string,
        public descripcion?: string,
        public status?: string,
        public students?: IStudent[],
        public reviews?: IReview[],
        public iteracion?: ISprint
    ) {}
}
