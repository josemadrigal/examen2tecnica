import { Moment } from 'moment';
import { IProyect } from 'app/shared/model/proyect.model';

export interface IPeriod {
    id?: number;
    startDate?: Moment;
    endate?: Moment;
    name?: string;
    proyects?: IProyect[];
}

export class Period implements IPeriod {
    constructor(
        public id?: number,
        public startDate?: Moment,
        public endate?: Moment,
        public name?: string,
        public proyects?: IProyect[]
    ) {}
}
