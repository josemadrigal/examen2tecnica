import { IStudent } from 'app/shared/model/student.model';
import { ISprint } from 'app/shared/model/sprint.model';
import { IProyect } from 'app/shared/model/proyect.model';

export interface ITeam {
    id?: number;
    name?: string;
    status?: string;
    students?: IStudent[];
    springs?: ISprint[];
    proyecto?: IProyect;
}

export class Team implements ITeam {
    constructor(
        public id?: number,
        public name?: string,
        public status?: string,
        public students?: IStudent[],
        public springs?: ISprint[],
        public proyecto?: IProyect
    ) {}
}
