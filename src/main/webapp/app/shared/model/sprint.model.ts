import { Moment } from 'moment';
import { IStory } from 'app/shared/model/story.model';
import { ITeam } from 'app/shared/model/team.model';

export interface ISprint {
    id?: number;
    name?: string;
    endDate?: Moment;
    startDate?: Moment;
    status?: string;
    stories?: IStory[];
    equipo?: ITeam;
}

export class Sprint implements ISprint {
    constructor(
        public id?: number,
        public name?: string,
        public endDate?: Moment,
        public startDate?: Moment,
        public status?: string,
        public stories?: IStory[],
        public equipo?: ITeam
    ) {}
}
