import { ITeam } from 'app/shared/model/team.model';
import { IPeriod } from 'app/shared/model/period.model';

export interface IProyect {
    id?: number;
    name?: string;
    numStudent?: number;
    schedule?: string;
    status?: string;
    teams?: ITeam[];
    periodo?: IPeriod;
}

export class Proyect implements IProyect {
    constructor(
        public id?: number,
        public name?: string,
        public numStudent?: number,
        public schedule?: string,
        public status?: string,
        public teams?: ITeam[],
        public periodo?: IPeriod
    ) {}
}
