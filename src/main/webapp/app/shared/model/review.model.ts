import { Moment } from 'moment';
import { IStory } from 'app/shared/model/story.model';

export interface IReview {
    id?: number;
    timestamp?: Moment;
    commnet?: string;
    status?: string;
    codigoHU?: IStory;
}

export class Review implements IReview {
    constructor(public id?: number, public timestamp?: Moment, public commnet?: string, public status?: string, public codigoHU?: IStory) {}
}
